function distance(first, second) {
  //TODO: implementați funcția
  // TODO: implement the function
  if (Array.isArray(first) && Array.isArray(second)) {
    let difFst = Array.from(new Set(first.filter(x => !second.includes(x))));
    let difSec = Array.from(new Set(second.filter(x => !first.includes(x))));
    let result = difFst.concat(difSec);
    return result.length;
  } else throw new Error("InvalidType");
}

module.exports.distance = distance;
